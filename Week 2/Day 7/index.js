// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const pyramid = require("./function/pyramidVolume"); // import pyramid
const prism = require("./function/prismVolume.js");
const cone = require("./function/coneVolume");
const tubeV = require("./function/tubeV");

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

// Function to display the menu
function menu() {
  console.log("Geometry Volume Calculator by Group 2");
  console.log(`                Menu                 `);
  console.log(`=====================================`);
  console.log(`1. Pyramid with square base`);
  console.log(`2. Volume of Prism`);
  console.log(`3. Cone`);
  console.log(`4. Tube`);
  console.log(`5. Exit`);
  rl.question(`Choose option: `, (option) => {
    if (!isNaN(option)) {
      // If option is a number it will go here
      if (option == 1) {
        pyramid.inputPyramidbased(); // It will call inputPyramidbased() function in pyramid file
      } else if (option == 2) {
        prism(); // It will call input() function in prismVolume file
      } else if (option == 3) {
        cone.inputCone();
      } else if (option == 4) {
        tubeV.inputTube();
      } else if (option == 5) {
        rl.close(); // It will close the program
      } else {
        console.log(`Option must be 1 to 6!\n`);
        menu(); // If option is not 1 to 3, it will go back to the menu again
      }
    } else {
      // If option is not a number it will go here
      console.log(`Option must be number!\n`);
      menu(); // If option is not 1 to 3, it will go back to the menu again
    }
  });
}

menu(); // call the menu function to display the menu

module.exports.rl = rl; // export rl to make another can run the readline
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
