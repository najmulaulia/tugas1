const index = require("../index");

function tube(r, h) {
  var phi = 3.14;
  return phi ** r * h;
}
function inputTube() {
  index.rl.question("radius : ", (r) => {
    index.rl.question("height : ", (h) => {
      if (r != NaN && h != NaN) {
        console.log(`\ntube: ${tube(r, h)} \n`);
        index.rl.close();
      } else {
        console.log(`height and radius must be a number\n`);
        inputTube();
      }
    });
  });
}
module.exports = { inputTube };
