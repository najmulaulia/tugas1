const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
var data2 = data.filter(i => typeof i === 'number');

// console.log(data2.join(' '));

// Should return array
function sortAscending(data2) {
   var swap, done = false,
   swapped;
   while(!done){
       swapped = 0;
       for(i = 1; i < data2.length; i++)
    if(data2[i - 1] > data2[i]){
        swap = data2[i];
        data2[i] = data2[i - 1];
        data2[i - 1] = swap;
        swapped = 1;
    };
    if (swapped == 0){
        done = true;
    };
    };
return data2;
};
console.log(sortAscending(data2));
// Should return array
function sortDecending(data2) {
    var swap, done = false,
    swapped;
    while(!done){
        swapped = 0;
        for(i = 0; i < data2.length; i++)
     if(data2[i + 1] > data2[i]){
         swap = data2[i];
         data2[i] = data2[i + 1];
         data2[i + 1] = swap;
         swapped = -1;
     };
     if (swapped == 0){
         done = true;
     };
     };
 return data2;
 };
 console.log(sortDecending(data2));


// DON'T CHANGE
test(sortAscending, sortDecending, data);
