const ThreeDimention = require("./threeDimention");
class Beam extends ThreeDimention{
    constructor(length, width, height){
        super("Beam");
        this.length = length;
        this.width = width;
        this.height = height;
    }
    introduce(who){
        super.introduce();
        console.log(`${who}, this is ${this.name}!`);
    }
    calculatevolume(){
        super.calculatevolume();
        let area = this.length * this.width * this.height;
        console.log(`${this.name} area is ${area} cm3 \n`);
    }
}
module.exports = Beam;
var volBeam = new Beam(5, 4, 3);
volBeam.introduce("Najmul");
volBeam.calculatevolume();