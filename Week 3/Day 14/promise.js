// const fs = require("fs");
// const readFile = (file, options) =>
// new Promise((success, failed)=>{
//     fs.readFile(file, options, (err, content)=>{
//         if(err) failed (err);
//         return success(content);
//     });
// });

// readFile("./contents/content1.txt", "utf-8")
// .then((content1) => {
//     console.log(content1);
//     return readFile("./contents/content2.txt", "utf-8");
// })
// .then((content2) =>{
//     console.log(content2);
// })
// .catch((err)=> console.log(err));  
const fs = require("fs");

// Make promise object
const readFile = (file, options) =>
  new Promise((success, failed) => {
    fs.readFile(file, options, (err, content) => {
      if (err) failed(err);
      return success(content);
    });
  });

const readAllFiles = async () => {
  try {
    let data = await Promise.all([
      readFile("./contents/content1.txt", "utf-8"),
      readFile("./contents/content2.txt", "utf-8"),
      readFile("./contents/file3.txt", "utf-8"),
      readFile("./contents/file4.txt", "utf-8"),
      readFile("./contents/file5.txt", "utf-8"),
      readFile("./contents/file6.txt", "utf-8"),
      readFile("./contents/file7.txt", "utf-8"),
      readFile("./contents/file8.txt", "utf-8"),
    ]);

    // let content1 = await readFile("./contents/content1.txt", "utf-8");
    // let content2 = await readFile("./contents/content2.txt", "utf-8");

    console.log(data[0] + " " + data[1] + " " + data[2] + " " + data[3] + " " + data[4] + " " + data[5] + " " + data[6] + " " + data[7]);
  } catch (e) {
    console.log("What the error!");
  }
};

readAllFiles();
