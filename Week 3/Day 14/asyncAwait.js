const fs = require("fs");
const readFile = (file, options) =>
  new Promise((success, failed) => {
    fs.readFile(file, options, (err, content) => {
      if (err) failed(err);
      return success(content);
    });
  });
  const readAllFiles = async () => {
    try {
    let content1 = await readFile("./contents/content1.txt", "utf-8");
    let content2 = await readFile("./contents/content2.txt", "utf-8");
    let content3 = await readFile("./contents/file3.txt", "utf-8");
    let content4 = await readFile("./contents/file4.txt", "utf-8");
    let content5 = await readFile("./contents/file5.txt", "utf-8");
    let content6 = await readFile("./contents/file6.txt", "utf-8");
    let content7 = await readFile("./contents/file7.txt", "utf-8");
    let content8 = await readFile("./contents/file8.txt", "utf-8");
    let total = content1 + " " + content2 + " " + content3 + " " + content4 + " " + content5 + " " + content6 + " " + content7 + " " + content8; 
      console.log(total);
    } catch (e) {
      console.log("What the error!");
    }
  };
  
  readAllFiles();  
