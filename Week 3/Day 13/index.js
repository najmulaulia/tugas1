const Cube = require("./Cube");
const Cone = require("./cone");
const Beam = require("./Beam");
const Tube = require("./tube");
module.exports = {Cube, Cone, Beam, Tube};
