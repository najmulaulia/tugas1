const Geometry = require("./geometry");
class ThreeDimention extends Geometry{
    constructor(name){
        super(name, "3D");
    }
    introduce(){
        super.introduce();
        console.log(`This is ${this.type}`);
    }
    calculatevolume(){
        console.log(`${this.name} volume`);
    }
  }
module.exports = ThreeDimention;
