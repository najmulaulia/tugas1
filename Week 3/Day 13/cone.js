const ThreeDimention = require("./threeDimention");
class Cone extends ThreeDimention{
    constructor(rad, height){
        super("Cone");
        this.rad = rad;
        this.height = height;
    }
    introduce(who){
        super.introduce();
        console.log(`${who}, this is ${this.name}!`);
    }
    calculatevolume(){
        super.calculatevolume();
           let area = 3.14 * 1/3 * this.rad * this.height;
        console.log(`${this.name} area is ${area} cm3 \n`);
    }
}
module.exports = Cone;
var volCone= new Cone(5, 10);
volCone.introduce("Najmul");
volCone.calculatevolume();
