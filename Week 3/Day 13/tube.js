const ThreeDimention = require("./threeDimention");
class Tube extends ThreeDimention{
    constructor(rad, height){
        super("Tube");
        this.rad = rad;
        this.height = height;
    }
    introduce(who){
        super.introduce();
        console.log(`${who}, this is ${this.name}!`);
    }
    calculatevolume(){
        super.calculatevolume();
           let area = 3.14 * this.rad * this.height;
        console.log(`${this.name} area is ${area} cm3 \n`);
    }
}
module.exports = Tube;
var volTube= new Tube(5, 10);
volTube.introduce("Najmul");
volTube.calculatevolume();
