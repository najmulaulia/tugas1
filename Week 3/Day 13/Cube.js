const ThreeDimention = require("./threeDimention");
class Cube extends ThreeDimention{
    constructor(sisi){
        super("Cube");
        this.sisi = sisi;
    }
    introduce(who){
        super.introduce();
        console.log(`${who}, this is ${this.name}!`);
    }
    calculatevolume(){
        super.calculatevolume();
        let area = this.sisi * this.sisi * this.sisi;
        console.log(`${this.name} area is ${area} cm3 \n`);
    }
}
module.exports = Cube;
var volCube = new Cube(5);
volCube.introduce("Najmul");
volCube.calculatevolume();
